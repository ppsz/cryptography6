#include "Main.h"

void Erastothenes(int last_number)
{
	std::vector<int> numbers, temp;
	int current_number = 2;
	int iterator = 0;

	for (int i = 2; i <= last_number; ++i)
		numbers.push_back(i);
	
	for (;;)
	{
		current_number = numbers[iterator];
		for (auto &number : numbers)
		{
			if (number == current_number)
				temp.push_back(number);
			else if ((number % current_number) != 0)
				temp.push_back(number);
		}
		numbers = temp;
		if (numbers.back() < std::pow(current_number, 2))
			break;
		temp.clear();
		++iterator;
	}

	for (auto number : numbers)
	{
		std::cout << number << ", ";
	}
}