#include "Main.h"

//void Euklides(int first, int second)
//{
//	while (first != second)
//	{
//		if (first < second)
//			second -= first;
//		else
//			first -= second;
//	}
//	std::cout << first << "\n";
//}

std::array<long long, 2> Euklides(long long first, long long second)
{
	long long r, gcd, a, q, b;
	long long x, x1, x2;
	long long y, y1, y2;
	std::array<long long, 2> result;

	// a must be greater than b
	if (second > first)
	{
		gcd = second;
		second = first;
		first = gcd;
	}

	//initialize a and b
	a = first;
	b = second;

	//initialize r and gcd
	q = a / b;
	r = a - q*b;
	gcd = b;

	//initialize x and y
	x2 = 1;
	x1 = 0;
	y2 = 0;
	y1 = 1;
	x = 1;
	y = y2 - (q - 1)*y1;

	while (r != 0)
	{
		a = b;
		b = r;

		x = x2 - q*x1;
		x2 = x1;
		x1 = x;

		y = y2 - q*y1;
		y2 = y1;
		y1 = y;

		gcd = r;
		q = a / b;
		r = a - q*b;
	}

	result[0] = gcd;
	result[1] = y;

	return result;

	//std::cout << "NWD(" << first << ", " << second << ") = " << gcd << " = " << x << " * " << first << " + " << y << " * " << second << "\n";

	/*if (gcd == 1)
		printf("%d * %d mod %d = 1", second, y, first);*/

}