#include "Main.h"

int main()
{
	long long P = 1489;
	long long Q = 2957;
	long long m = 1969;
	long long c;
	long long result;
	//Erastothenes(1000);
	//Euklides(77654321, 1289);

	c = RSA(P, Q, m, 0);
	result = RSA(P, Q, c, 1);

	std::cout << "message: " << m << "\n" << "encrypted: " << c << "\n" << "check: " << result << "\n";

	system("pause");
}