#include "Main.h"

long long RSA(long long P, long long Q, long long input_message, int encrypt_decrypt)
{
	long long N = P*Q;
	long long fiN = (P - 1)*(Q - 1);
	static long long E;
	static long long D;
	long long result;
	std::array<long long, 2> euklides;

	if (encrypt_decrypt == 0)
	{
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_int_distribution<long long> dist(1, fiN);

		do
		{
			E = dist(mt);
			euklides = Euklides(E, fiN);
		} while (euklides[0] != 1);

		D = euklides[1];
		while (D < 0)
			D += fiN;
		result = ExpMod(input_message, E, N);
	}

	if (encrypt_decrypt == 1)
		result = ExpMod(input_message, D, N);

	return result;
}