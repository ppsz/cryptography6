#pragma once
#include <iostream>
#include <cmath>
#include <vector>
#include <array>
#include <random>
#include <bitset>

void Erastothenes(long long last_number);
std::array<long long, 2> Euklides(long long first, long long second);
long long RSA(long long P, long long Q, long long input_message, int encrypt_decrypt);
unsigned long long ExpMod(unsigned long long base, unsigned long long exponent, unsigned long long modulo);